﻿using System.Diagnostics;
using System.Reflection;
using SQLiteNetExtensions.Attributes;
#if USING_MVVMCROSS
using SQLiteConnection = Cirrious.MvvmCross.Community.Plugins.Sqlite.ISQLiteConnection;
#elif PCL
using SQLite.Net;
#else
using SQLite;
#endif


namespace SQLiteNetExtensions.Extensions.TextBlob
{
    public static class TextBlobOperations
    {
        private static ITextBlobSerializer _serializer;

        public static void SetTextSerializer(ITextBlobSerializer serializer)
        {
            _serializer = serializer;
        }

        public static ITextBlobSerializer GetTextSerializer()
        {
#if NO_DEFAULTSERIALIZER
            return _serializer;
#else
            // If not specified, use default JSON serializer
            return _serializer ?? (_serializer = new Serializers.JsonBlobSerializer());
#endif
        }

        public static void GetTextBlobChild(object element, PropertyInfo relationshipProperty)
        {
            var type = element.GetType();
            var relationshipType = relationshipProperty.PropertyType;

            Debug.Assert(relationshipType != typeof(string), "TextBlob property is already a string");

            var textblobAttribute = relationshipProperty.GetAttribute<TextBlobAttribute>();
            var textProperty = type.GetRuntimeProperty(textblobAttribute.TextProperty);
            Debug.Assert(textProperty != null && textProperty.PropertyType == typeof(string), "Text property for TextBlob relationship not found");
            
            var textValue = (string)textProperty.GetValue(element, null);
            var textSerializer = GetTextSerializer();
            var value = textValue != null && textSerializer != null ? textSerializer.Deserialize(textValue, relationshipType) : null;

            relationshipProperty.SetValue(element, value, null);
        }

        public static void UpdateTextBlobProperty(object element, PropertyInfo relationshipProperty)
        {
            var type = element.GetType();
            var relationshipType = relationshipProperty.PropertyType;

            Debug.Assert(relationshipType != typeof(string), "TextBlob property is already a string");

            var textblobAttribute = relationshipProperty.GetAttribute<TextBlobAttribute>();
            var textProperty = type.GetRuntimeProperty(textblobAttribute.TextProperty);
            Debug.Assert(textProperty != null && textProperty.PropertyType == typeof(string), "Text property for TextBlob relationship not found");

            var value = relationshipProperty.GetValue(element, null);
            var textSerializer = GetTextSerializer();
            var textValue = value != null && textSerializer != null ? textSerializer.Serialize(value) : null;

            textProperty.SetValue(element, textValue, null);
        }
    }
}
